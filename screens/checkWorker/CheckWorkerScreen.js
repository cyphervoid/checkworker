import React, { Component } from "react";
import { StyleSheet, AsyncStorage } from "react-native";
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { withNavigationFocus } from 'react-navigation';

import Camera from './Camera';
import ShowImageOption from "./ShowImageOption";

import { firestore, storage } from '../../utils/firebase';
import { WORKPLACE, USER } from "../config/config";


class CheckWorkerScreen extends Component {
  static navigationOptions = {
    title: "เช็คชื่อ",
    tabBarIcon: ({ tintColor }) => (
      <FontAwesome5 name="user-check" size={23}  color={tintColor} />
    )
  };

  state = {
    picturePath: null,
    filteredText: '',
    selectedWorker: null,
    checkType: false,
    userId: null,
    workerList: [],
  };

  async componentDidMount() {
    const editTxaction = this.props.navigation.getParam('editTxaction');
    if (editTxaction) {
      // Handle Edit Image Case 
    }
    else {
      const workplaceId = await AsyncStorage.getItem(WORKPLACE)
      const userId = await AsyncStorage.getItem(USER)
      firestore
        .collection('Worker')
        .where('role', '==', 'WORKER')
        .where('status', '==', 'WORKING')
        .where('workplaceId', '==', workplaceId)
        .onSnapshot(querySnapshot => {
          let workerList = []
          querySnapshot.forEach(doc => {
            workerList.push({
              id: doc.id,
              name: doc.data().nickname + ' ' + doc.data().firstname + ' ' + doc.data().lastname,
              ...doc.data()
            });
          });
          this.setState({...this.state, workerList, userId})
        })
      }
  }

  takePicture = async (camera) => {
    if (camera) {
      const options = { quality: 0.5, base64: true, fixOrientation: true };
      const data = await camera.takePictureAsync(options);
      this.setState({...this.state, picturePath: data.uri});
    }
  };

  handleOnTextChange = (text) => {
    this.setState({...this.state, filteredText: text})
  }

  handleOnItemSelect = (item) => {
    this.setState({...this.state, selectedWorker: item})
  }

  handleAbortPicture = () => {
    this.setState({
      ...this.state,
      picturePath: null,
      selectedWorker: null
    })
  };

  handleSavePicture = () => {
    const worker = this.state.selectedWorker
    if (worker) {
      if (worker.recentTxaction) {
        worker.recentTxaction
          .get()
          .then(doc => {
            const cur = new Date();
            const recentTxaction = doc.data()
            if (recentTxaction.date.getDate() == cur.getDate() && 
                recentTxaction.date.getMonth() == cur.getMonth() && 
                recentTxaction.date.getFullYear() == cur.getFullYear()) {
              if (recentTxaction.status === 'COMPLETE')
                  alert('การลงชื่อครบถ้วนแล้ว')
              else
                this.checkOut(worker.recentTxaction, this.state.picturePath)
            }
            else {
              this.checkIn(this.state.userId, worker.id, this.state.picturePath)
            }
          })
          .catch(error => {
            console.log("Error getting documents: ", error);
          });
      }
      else {
        this.checkIn(this.state.userId, worker.id, this.state.picturePath)
      }
    }
    else
      alert("กรุณาเลือกพนักงาน")
  };

  checkIn = (ownerId, workerId, picturePath) => {
    let docRef = firestore.collection('TxactionCheckWorker').doc()
    let imgRef = storage.ref().child(`TxactionCheckWorker/checkIn${docRef.id}.jpg`)
    imgRef
      .put(picturePath)
      .then(snapshot =>
        imgRef.getDownloadURL()
      )
      .then((url) => {
        docRef.set({
          checkIn: {
            image: url,
            timestamp: new Date()
          },
          status: 'CHECKIN',
          date: new Date(),
          ownerId,
          workerId,
        })
      })
      .then(() => {
        firestore
          .collection('Worker')
          .doc(workerId)
          .update({
            recentTxaction: docRef
          })
      })
      .then(() => {
        alert('เช็คชื่อ "เข้างาน" เสร็จสิ้น')
        this.setState({...this.state, picturePath: null})
      })
      .catch(error => {
          console.error("Error adding document: ", error);
      });
  }

  checkOut = (docRef, picturePath) => {
    let imgRef = storage.ref().child(`TxactionCheckWorker/checkOut${docRef.id}.jpg`)
    imgRef
      .put(picturePath)
      .then(snapshot =>
        imgRef.getDownloadURL()
      )
      .then((url) => {
        docRef.update({
          checkOut: {
            image: url,
            timestamp: new Date()
          },
          status: 'COMPLETE',
        })
      })
      .then(() => {
        alert('เช็คชื่อ "ออกงาน" เสร็จสิ้น')
        this.setState({...this.state, picturePath: null})
      })
      .catch(error => {
          console.error("Error adding document: ", error);
      });
  }

  updateTransaction = () => {
    // Handle Edit Image Case
  };

  render() {
    const isFocused = this.props.navigation.isFocused();
    if(isFocused) {
      const { picturePath, workerList } = this.state;
      if (picturePath) 
        return <ShowImageOption 
                  picturePath={picturePath}
                  workerList={workerList}
                  handleOnTextChange={this.handleOnTextChange}
                  handleOnItemSelect={this.handleOnItemSelect}
                  handleAbortPicture={this.handleAbortPicture}
                  handleSavePicture={this.handleSavePicture}
                />
      else
        return <Camera takePicture={this.takePicture}/>
    }
    else {
      return null;
    }
    
  }
  
}

const styles = StyleSheet.create({
  icon: {
    width: 26,
    height: 26
  },
});


export default withNavigationFocus(CheckWorkerScreen);