import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import { RNCamera } from 'react-native-camera';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export default Camera = ({takePicture}) => {
    let camera;
    return (
        <View style={styles.container}>
            <RNCamera
                ref={(cam) => {
                    camera = cam
                }}
                style={styles.preview}
                permissionDialogTitle={'Permission to use camera'}
                permissionDialogMessage={'We need your permission to use your camera phone'}
                type={RNCamera.Constants.Type.back}
                autoFocus={RNCamera.Constants.AutoFocus.on}
                flashMode={RNCamera.Constants.FlashMode.auto}
            >
                <TouchableOpacity
                onPress={() => takePicture(camera)}
                style = {styles.capture}
                >
                <MaterialIcons name='camera' size={70}  color='white' />
                </TouchableOpacity>
            </RNCamera>
        </View>
    );
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  
});