import React, { Component } from "react";
import { StyleSheet, View, Image, TouchableOpacity } from "react-native";
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import SearchableDropdown from 'react-native-searchable-dropdown';

export default ShowImageOption = (props) => {
    const {
        picturePath,
        workerList,
        handleOnTextChange,
        handleOnItemSelect,
        
        handleAbortPicture,
        handleSavePicture,
    } = props;
    return (
        <View style={styles.container}>
            <View style={styles.picture}>
                <Image
                    source={{uri: picturePath}}
                    style={{flex: 1}}
                />
            </View>
            <View style={styles.topbar}>
                <SearchableDropdown
                    items={workerList}
                    placeholder='ค้นหาคนงาน'
                    onTextChange={text => {handleOnTextChange(text)}}
                    onItemSelect={item => {handleOnItemSelect(item)}}
                    containerStyle={{ 
                        backgroundColor: 'white',
                        flex: 1
                    }}
                    textInputStyle={{
                        padding: 12,
                        borderWidth: 1,
                        borderColor: '#ccc',
                        borderRadius: 5,
                    }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#ddd',
                        borderColor: '#bbb',
                        borderWidth: 1,
                        borderRadius: 5,
                    }}
                    itemTextStyle={{ 
                        color: '#222' 
                    }}
                    itemsContainerStyle={{ 
                        maxHeight: 150
                    }}
                    resetValue={false}
                    underlineColorAndroid='transparent'
                />
            </View>
            <View style={styles.bottombar}>
                <TouchableOpacity
                    onPress={handleAbortPicture}
                >
                    <View style={{backgroundColor: 'red', borderRadius: 15}}>
                    <MaterialIcons name='cancel' size={50}  color='white' />
                    </View>
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={handleSavePicture}
                >
                    <View style={{backgroundColor: 'green', borderRadius: 15}}>
                    <MaterialIcons name='save' size={50}  color='white' />
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black'
  },
  icon: {
    width: 26,
    height: 26
  },
  picture: {
    ...StyleSheet.absoluteFillObject,
  },
  topbar: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
  },

  bottombar: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    padding: 10,
  },
  
});
