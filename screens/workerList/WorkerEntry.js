import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from "react-native";

export default (WorkerEntry = ({
  id,
  firstname,
  lastname,
  sex,
  nickname,
  navigation
}) => (
  <TouchableHighlight
    onPress={() => {
      console.log("id :", id);
      navigation.setParams({
        workerDetailID: id
      });
      navigation.navigate("WorkerDetailScreen", {
        workerDetailID: id
      });
    }}
  >
    <View style={styles.container}>
      <View style={styles.content}>
        <Text style={styles.text}>{nickname}</Text>
        <Text style={styles.text}>
          {sex == "ชาย" ? "นาย" : sex == "หญิง" ? "นางสาว" : ""}
          {firstname && lastname ? (
            <Text style={styles.text}>
              {firstname} {lastname}
            </Text>
          ) : (
            <Text />
          )}
        </Text>
      </View>
    </View>
  </TouchableHighlight>
));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "space-between",
    backgroundColor: "white",
    flexDirection: "row",
    borderColor: "gray",
    borderBottomWidth: 1
  },
  checkWork: {
    flexDirection: "row",
    alignItems: "center",
    flex: 1
  },
  content: {
    flex: 3,
    flexDirection: "row",
    alignItems: "center",
    height: 80,
    width: 150
  },
  text: {
    fontSize: 15,
    marginLeft: 10
  }
});
