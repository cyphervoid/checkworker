import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image, FlatList } from "react-native";
import WorkerEntry from './WorkerEntry'
export default  WorkerList = ({ worker , navigation}) => (  

      <View style={styles.container}>
        <FlatList
          data={worker}
          keyExtractor={(item) => item.id}
          renderItem={({ item }) => <WorkerEntry navigation = {navigation} {...item} />}
        />
      </View>

)
const styles = StyleSheet.create({
  container: {
    flex: 3,
    justifyContent: "center",
    backgroundColor: "#F5FCFF",
    width: '100%'
  },
  icon: {
    width: 26,
    height: 26
  }
});
