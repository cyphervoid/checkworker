import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image, AsyncStorage, Button, TouchableHighlight, TouchableOpacity } from "react-native";
import firebase from "react-native-firebase";
import ManagerList from "./ManagerList";
import WorkerList from "./WorkerList";
import Icon from "react-native-vector-icons/FontAwesome5";
import { USER, WORKPLACE } from "../config/config";
import { firestore, auth } from '../../utils/firebase'
import _ from "lodash";
import { Input } from "native-base";
import { StackActions, NavigationActions } from 'react-navigation';
export default class ListScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    title: "รายชื่อพนักงาน",
    headerRight: (
      <TouchableOpacity
        onPress={() =>
          navigation.navigate(
            "CheckWorkerScreen",
            navigation.getParam("config")
          )
        }
        style={{marginRight: 15}}
      >
        <Icon name='camera-retro' size={30} color='black'/>
      </TouchableOpacity>
    )
  });

  state = {
    worker: [],
    manager: {},
    workplace: { adress: {} },
    filterWorker: []
  };

  getManager = userId => {
    return firestore
      .collection("Worker")
      .doc(userId)
      .get();
  };

  getWorker = workplaceId => {
    return firestore
      .collection("Worker")
      .where("role", "==", "WORKER")
      .where("workplaceId", "==", workplaceId)
      .where("status", "==", "WORKING")
      .get();
  };

  getWorkplace = workplaceId => {
    return firestore
      .collection("Workplace")
      .doc(workplaceId)
      .get();
  };

  subscribeToManager = async () => {
    const workplaceId = await AsyncStorage.getItem(WORKPLACE);
    const userId = await AsyncStorage.getItem(USER);

    Promise.all([
      this.getManager(userId),
      this.getWorker(workplaceId),
      this.getWorkplace(workplaceId)
    ]).then((values) => {
      let manager
      let worker = []
      let workplace
      values.forEach(async (value, index) => {
        if (index == 0) {

          manager = {
            id: value.id,
            ...value.data()
          }
        } else if (index == 1) {
          value.forEach(doc => {
            worker.push({
              id: doc.id,
              ...doc.data()
            });
          })
        } else if (index == 2) {
          workplace = {
            id: value.id,
            ...value.data()
          }
        }

      })
      this.setState({
        manager,
        workplace,
        worker,
        filterWorker: worker
      });

    }).catch(function (error) {
      console.log("Error getting documents: ", error);
    });

  }

  handleInputChange = value => {
    this.setState({
      ...this.state,
      filterWorker: _.filter(this.state.worker, o => {
        if (value != " ") return o.firstname.includes(value);
        else return false;
      })
    });
  }

  componentDidMount() {
    this.subscribeToManager();
  }
  componentWillUnmount() { }

  onLogOut = () => {
    const { navigation } = this.props
    auth.signOut().then(() => {
      console.log('logOut :');
      AsyncStorage.clear();
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: 'LoginScreen' })],
      });
      navigation.dispatch(resetAction);
    }).catch((error) => {
      console.log('error :', error);
    })
  }

  render() {
    const { manager, workplace, filterWorker } = this.state;
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <TouchableHighlight
          onPress={this.onLogOut}
          style={styles.manager}
        >
          <ManagerList manager={manager} workplace={workplace} />
        </TouchableHighlight>
        <View style={styles.containerSearchBar}>
          <Input
            placeholder="ค้นหาพนักงาน"
            onChangeText={this.handleInputChange}
            style={styles.searchbar}
          />
        </View>
        <WorkerList navigation={navigation} worker={filterWorker} />
      </View >
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  manager: {
    flex: 1,
    backgroundColor: "red"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  worker: {
    flex: 3,
    backgroundColor: "yellow"
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  icon: {
    width: 26,
    height: 26
  },
  searchbar: {
    backgroundColor: "#F5F5F5"
  },
  containerSearchBar: {
    height: 50,
    width: "100%"
  }
});
