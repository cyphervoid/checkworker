import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Image } from "react-native";

export default class ManagerList extends Component {
  componentDidMount() {
    console.log("all Work");
  }

  render() {
    const { manager , workplace } = this.props;
    const {
      image
    } = manager;
    return (
      <View style={styles.container}>
        <View style={styles.inner}>
          <View
            style={{ flex: 2, alignItems: "center", justifyContent: "center" }}
          >
            <Image
              source={image ? { uri: image } : null}
              style={{ width: 75, height: 75, borderRadius: 50 }}
            />
          </View>
          <View style={styles.info}>
            <Text style={styles.headText}>ชื่อ: {manager.firstname} {manager.lastname}</Text>
            <Text style={styles.headText}>เพศ: {manager.sex}</Text>
            <Text style={styles.headText}>ตำแหน่ง: {manager.role}</Text>
          </View>
          <View style={styles.workPlace}>
            <Text style={styles.textWorkPlace}>สังกัด</Text>
            <Text style={styles.textWorkPlace}>{workplace.name}</Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    width: 350,
    backgroundColor: "#F5FCFF"
  },
  inner: {
    flexDirection: "row",
    width: 350,
    height: 125,
    backgroundColor: "#EFF1F2",
    borderRadius: 20
  },
  info: {
    flex: 3,
    marginLeft: 5,
    justifyContent: "center"
  },
  headText: {
    fontWeight: "bold",
    marginTop: 5
  },
  workPlace: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column"
  },
  textWorkPlace: {
    fontWeight: "bold",
    fontSize: 14,
    textAlign: "center"
  },
  icon: {
    width: 26,
    height: 26
  }
});
