import { createStackNavigator, createAppContainer, createBottomTabNavigator } from 'react-navigation';
import LoginScreen from '../login/LoginScreen'
import WorkerDetailScreen from '../workerDetail/WorkerDetailScreen'
import CheckWorkerScreen from '../checkWorker/CheckWorkerScreen';
import Initial from './Initial';
import ListScreen from '../workerList';

const AppNavigator = createStackNavigator({
    InitialScreen: {
        screen: Initial
    },
    LoginScreen: {
        screen: LoginScreen
    },
    WorkerListScreen: {
        screen: ListScreen
    },
    WorkerDetailScreen: {
        screen: WorkerDetailScreen
    },
    CheckWorkerScreen: {
        screen: CheckWorkerScreen,
    }
}, {
        initialRouteName: "InitialScreen",

    });



export const Rootstack = createAppContainer(AppNavigator)
