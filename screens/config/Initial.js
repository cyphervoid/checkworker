
import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { Spinner } from 'native-base';
import { auth, firestore } from '../../utils/firebase'
import { USER, WORKPLACE } from './config';
import { setKeyStoarage } from '../../utils/setItemStorage';


export default class Initial extends Component {

    state = {
        loading: true,
        user: null
    }
    componentDidMount() {
        const { navigation } = this.props

        this.subscripToAuth()

    }

    subscripToAuth = () => {
        const { navigation } = this.props
        let config = {}
        this.authSubscription = auth.onAuthStateChanged((user) => {
            if (user) {
                this.getUserInfo(user).then((user) => {
                    if (user) {
                        let workplaceRef = user.data().workplace
                        config.user = {
                            id: user.id,
                            ...user.data(),
                        }
                        setKeyStoarage(USER, user.id)
                        if (workplaceRef) return this.getWorkplaceInfo(workplaceRef)
                        else return new Promise()
                    } else {
                        navigation.replace("LoginScreen")
                    }
                }).then((workplaceDoc) => {
                    config.workplace = {
                        id: workplaceDoc.id,
                        ...workplaceDoc.data()
                    }
                    setKeyStoarage(WORKPLACE, workplaceDoc.id)
                    navigation.setParams({ config })
                    navigation.replace('WorkerListScreen', {
                        config
                    })
                }).catch((error) => {
                    console.log('error user :', error);
                })
            } else {
                navigation.replace("LoginScreen")
            }
        })
    }

    getUserInfo = (user) => {
        if (!user) return
        const collection = firestore.collection("Worker")
        const docRef = collection.doc(user.uid)
        return docRef.get()
    }

    getWorkplaceInfo = (workplaceRef) => {
        if (!workplaceRef) return
        return workplaceRef.get()
    }

    async componentWillUnmount() {
        this.authSubscription()
    }
    render() {
        return (
            <View style={styles.container}>
                <Spinner color='red' />
            </View>

        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
