import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    Image
} from "react-native";
import { auth, firestore } from "../../utils/firebase";

class LoginScreen extends Component {
    state = {
        email: "",
        password: "",
        submited: false,
        error: {
            message: ""
        }
    };

    onEmailChange = value => {
        this.setState({
            email: value
        });
    };

    onPasswordChange = value => {
        this.setState({
            password: value
        });
    };

    onLogin = async () => {
        const { email, password } = this.state
        const { navigation } = this.props
        let _email = email
        let _password = password
        if (email == "") _email = "admin@hotmail.com"
        if (password == "") _password = "1234567890"
        auth.signInWithEmailAndPassword(_email, _password).then((user) => {
            if (user) {
                navigation.replace("InitialScreen")
            } else {
                navigation.replace('Login')
            }
        }).catch((error) => {
            console.log(error)
            alert('Authentication Failed.')
        })
    }


    getUserInfo = async user => {
        if (!user) return;
        const collection = firestore.collection("Worker");
        const docRef = collection.doc(user.uid);
        return docRef.get();
    };
    render() {
        const { navigate } = this.props.navigation;
        return (
            <View style={styles.container}>
                <Image
                    source={require("../../asset/logoCo_A.png")}
                    style={{ width: 300, height: 200 }}
                />
                <TextInput
                    style={styles.textInput}
                    onChangeText={this.onEmailChange}
                    value={this.state.email}
                    placeholder="ลงชื่อเข้าใช้"
                />
                <TextInput
                    secureTextEntry
                    style={styles.textInput}
                    onChangeText={this.onPasswordChange}
                    value={this.state.password}
                    placeholder="รหัสผ่าน"
                />
                <Button title="เข้าสู่ระบบ" onPress={this.onLogin} />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: 'center',
        alignItems: "center",
        paddingTop: 30
    },
    welcome: {
        fontSize: 20,
        textAlign: "center",
        margin: 10
    },
    instructions: {
        textAlign: "center",
        color: "#333333",
        marginBottom: 5
    },
    textInput: {
        height: 40,
        width: "70%",
        borderColor: "gray",
        borderWidth: 2,
        marginTop: 8,
        marginBottom: 5
    }
});

export default LoginScreen;
