import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, TouchableHighlight } from "react-native";
import { firestore } from '../../utils/firebase'
import Icon from 'react-native-vector-icons/FontAwesome'

export default WorkerDetailEntryTxaction = ({ id, status, date, checkIn, checkOut, showModal }) => {

    return <TouchableHighlight onPress={() => showModal(id)}>
        <View style={styles.container}>
            <View style={styles.content}>
                <Text style={{ fontSize: 14 }}>เวลาเข้าทำงาน </Text>
                {checkIn ?
                    <Text style={styles.text}>{checkIn.timestamp.getDate()}/{checkIn.timestamp.getMonth()}/{checkIn.timestamp.getFullYear()}
                        {' '}{checkIn.timestamp.getHours()}-{checkIn.timestamp.getMinutes()}-{checkIn.timestamp.getSeconds()}</Text>
                    : <Text></Text>
                }
            </View>
            <View style={styles.content}>
                <Text style={{ fontSize: 14 }}>เวลาออกงาน </Text>
                {checkOut ?
                    <Text>{checkOut.timestamp.getDate()}/{checkOut.timestamp.getMonth()}/{checkOut.timestamp.getFullYear()}
                        {' '}{checkOut.timestamp.getHours()}-{checkOut.timestamp.getMinutes()}-{checkOut.timestamp.getSeconds()}</Text>
                    : <Text></Text>
                }
            </View>
            <View style={styles.content}>
                {status == "COMPLETE" ? <Icon style={styles.icon} name="check-square" size={25} color='green'/> : <Icon name="warning" size={25} color='orange'/>}
            </View>
        </View>
    </TouchableHighlight>

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "space-between",
        flexDirection: "row",
        borderRadius: 4,
        borderWidth: 1,
        borderColor: '#d6d7da',
    },
    content: {
        height: 50,
        width: 150,
        fontSize: 10,
        marginLeft: 5,
        marginTop: 8
    },
    text: {
        fontSize: 15
    },
    icon: {
        marginTop: 10
    }

});
