import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, FlatList, List } from "react-native";
import { firestore } from "../../utils/firebase";
import WorkerDetailEntryTxaction from "./WorkerDetailEntryTxaction";

export default WorkerDetailListTxaction = ({ txactions, showModal }) => (
    <FlatList
      data={txactions}
      keyExtractor={item => item.id}
      renderItem={({ item }) => (
        <WorkerDetailEntryTxaction showModal={showModal} {...item} />
      )}
    />
)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  }
});
