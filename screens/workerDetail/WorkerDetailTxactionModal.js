import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, FlatList, Modal, Image, TouchableHighlight } from "react-native";

export default WorkerDetailTxactionModal = ({ id, display, checkIn, checkOut, setModalVisible, handleEditTransaction }) => (
    <View>
        <Modal
            animationType="fade"
            transparent={true}
            visible={display}
            onRequestClose={() => {
                setModalVisible(display)
            }}>
            <View style={styles.container}>
                <TouchableHighlight onPress={() => handleEditTransaction(id, "checkIn")}>
                    <View>
                        <Text style={styles.text}> ลงชื่อเข้าทำงาน</Text>

                        {checkIn ?
                            <React.Fragment>
                                <Text style={styles.text}>{checkIn.timestamp.getDate()}/{checkIn.timestamp.getMonth()}/{checkIn.timestamp.getFullYear()}
                                    {' '}{checkIn.timestamp.getHours()}-{checkIn.timestamp.getMinutes()}-{checkIn.timestamp.getSeconds()}</Text>
                                <Image
                                    style={styles.image}
                                    source={{ uri: checkIn.image }}
                                />
                            </React.Fragment>
                            : <Text>Test</Text>
                        }
                    </View>

                </TouchableHighlight>

                <TouchableHighlight onPress={() => handleEditTransaction(id, "checkOut")}>
                    <View>
                        <Text style={styles.text}> ลงชื่อออกงาน</Text>

                        {checkOut ?
                            <React.Fragment>
                                <Text style={styles.text}>{checkOut.timestamp.getDate()}/{checkOut.timestamp.getMonth()}/{checkOut.timestamp.getFullYear()}
                                    {' '}{checkOut.timestamp.getHours()}-{checkOut.timestamp.getMinutes()}-{checkOut.timestamp.getSeconds()}</Text>
                                <Image
                                    style={styles.image}
                                    source={{ uri: checkOut.image }}
                                />
                            </React.Fragment>
                            : <Text>Test</Text>
                        }
                    </View>

                </TouchableHighlight>

            </View>
        </Modal>
    </View>
)


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#F5FCFF"
    },
    image: {
        width: 200,
        height: 200
    },
    text: {
        fontSize: 15
    },
});