import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button, Image } from "react-native";
import { Spinner } from 'native-base';
import { firestore } from '../../utils/firebase'
import TabPaneBar from './TabPaneBar'
import WorkerDetailEntryInformation from "./WorkerDetailEntryInformation";
import WorkerDetailTxactionModal from "./WorkerDetailTxactionModal";
import WorkerDetailListTxaction from "./WorkerDetailListTxaction";

export default class WorkerDetailScreen extends Component {

  state = {
    loading: true,
    userInfo: null,
    tab: 1,
    txactions: [],
    display: false
  }
  static navigationOptions = {
    title: "ข้อมูลพนักงาน",
  };

  subscribeWorker = (workerId) => {
    return firestore.collection("Worker").doc(workerId).onSnapshot((docWorker) => {
      if (docWorker) {
        this.setState({
          loading: false,
          userInfo: {
            id: docWorker.id,
            ...docWorker.data()
          },
        })
      }
    })
  }

  subscribeTxaction = (workerId) => {
    return firestore.collection("TxactionCheckWorker").where("workerId", "==", workerId)
      .onSnapshot((querySnapshot) => {
        let txactions = []
        querySnapshot.forEach(doc => {
          txactions.push({
            id: doc.id,
            ...doc.data()
          })
        })
        this.setState({
          txactions
        })
      })
  }
  componentDidMount() {
    const workerId = this.props.navigation.getParam("workerDetailID")
    let _workerId = workerId
    if (!workerId) _workerId = "DIvF84owTvbrBRsIewic"

    this.subscribeToWorker = this.subscribeWorker(_workerId)
    this.subscribeToTxaction = this.subscribeTxaction(workerId)

  }

  showModal = (txactionId) => {
    const { txactions } = this.state
    if (!txactions) return
    const txaction = txactions.find((txaction) => txaction.id == txactionId)
    this.setState({
      display: true,
      txaction
    })
  }

  handleEditTransaction = (txactionId, check) => {
    const { navigation } = this.props
  }

  setModalVisible = (display) => {
    this.setState({
      display: !display
    })
  }

  componentWillUnmount() {
    this.subscribeToWorker()
    this.subscribeToTxaction()
  }


  handleChageTab = (tab) => {

    this.setState({ tab })
  }

  displayComponent = () => {
    const { tab, txactions, userInfo } = this.state
    if (tab == 1) return <WorkerDetailEntryInformation {...userInfo} />
    else if (tab == 2) return <WorkerDetailListTxaction showModal={this.showModal} txactions={txactions} />
  }

  render() {
    let { userInfo, txaction, loading, tab } = this.state
    if (loading) return <Spinner color='red' />
    return (
      <View style={styles.container}>
        <WorkerDetailTxactionModal handleEditTransaction={this.handleEditTransaction} setModalVisible={this.setModalVisible} display={this.state.display} {...txaction} />
        <View style={styles.tabpane}>
          <TabPaneBar tab={tab} handleChageTab={this.handleChageTab} />
        </View>
        <View style={styles.content}>
          {this.displayComponent()}
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "center",
    // alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  tabpane: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#DCDCDC",
  },
  content: {
    flex: 10
  },
  modal: {
    flex: 1
  }
});
