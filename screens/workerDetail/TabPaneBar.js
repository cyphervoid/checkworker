import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Button } from "react-native";
import { firestore } from '../../utils/firebase'

export default TabPaneBar = ({ handleChageTab, tab }) => (
    <React.Fragment>
        <View style={tab == 1 ? styles.buttonHighlight : styles.button}>
            <Text onPress={() => handleChageTab("1")}>ข้อมูลทั่วไป</Text>
        </View>
        <View style={tab == 2 ? styles.buttonHighlight : styles.button}>
            <Text onPress={() => handleChageTab("2")} >ประวัติการเข้าทำงาน</Text>
        </View>
    </React.Fragment>
)



const styles = StyleSheet.create({
    container: {
        flex: 1,
        // justifyContent: "center",
        // alignItems: "center",
        backgroundColor: "#DCDCDC",
        flexDirection: "column"
    },
    button: {
        flex: 1,
        backgroundColor: "#DCDCDC",
        alignItems: "center",
        justifyContent: "center",
        fontSize: 20,
    },
    buttonHighlight: {
        flex: 1,
        backgroundColor: "#DCDCDC",
        alignItems: "center",
        justifyContent: "center",
        fontSize: 20,
        backgroundColor: "lightblue"
    },

});