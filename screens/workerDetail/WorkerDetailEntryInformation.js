import React, { Component } from "react";
import { Platform, StyleSheet, Text, View,TouchableHighlight } from "react-native";
import { firestore } from '../../utils/firebase'

export default WorkerDetailEntryInformation = ({ age, lastname, nickname, role, status, firstname }) => (

    <View style={styles.container}>
        <View style={[{flex: 1}, styles.elementsContainer]}>   
            <View style={{flex: 1, backgroundColor: '#FAF0E6'}}>
                <Text style={styles.text1}>ชื่อ-สกุล</Text>
                <View style={[{flexDirection:'row'}]}>
                    <View style={{width: 80, height: 35, backgroundColor: '#FAF0E6'}} >
                        <Text style={styles.text}>{firstname}</Text>
                    </View>
                    <View style={{width: 80, height: 35, backgroundColor: '#FAF0E6'}}>
                        <Text style={styles.text}>{lastname}</Text>
                    </View>
                </View>
                <View style={[{flexDirection:'row'}]}>
                    <View style={{width: 80, height: 35, backgroundColor: '#FAF0E6'}} >
                        <Text style={styles.text2}>ชื่อเล่น</Text>
                    </View>
                    <View style={{width: 130, height: 35, backgroundColor: '#FAF0E6'}}>
                        <Text style={styles.text3}>อายุ</Text>
                    </View>
                </View>
                <View style={[{flexDirection:'row'}]}>
                    <View style={{width: 80, height: 35, backgroundColor: '#FAF0E6'}} >
                        <Text style={styles.text4}>{nickname}</Text>
                    </View>
                    <View style={{width: 140, height: 35, backgroundColor: '#FAF0E6'}}>
                        <Text style={styles.text5}>{age}</Text>
                    </View>
                </View>
                <Text style={styles.text6}>หน้าที่/สถานะ</Text>
                <Text style={styles.text7}>{role} / {status}</Text>
            </View>

    
    
    
        </View>       
    </View>

)

const styles = StyleSheet.create({
    container: {
        marginTop: 150,
        flex: 1
      },
    text: {
        fontSize: 18,
        marginTop: 5,
        marginLeft: 13
    },
    text6: {
        marginLeft: 10,
        fontSize: 15
    },
    text1: {
        marginTop: 20,
        marginLeft: 10,
        fontSize: 15
    },
    text2: {
        marginTop: 5,
        marginLeft: 10,
        fontSize: 15
    },
    text3: {
        marginTop: 5,
        marginLeft: 90,
        fontSize: 15
    },
    text4: {
        fontSize: 18,
        marginLeft: 13
    },
    text5: {
        fontSize: 18,
        marginLeft: 93
    },
    text7: {
        marginTop: 5,
        fontSize: 18,
        marginLeft: 13
    },
    elementsContainer: {
        backgroundColor: '#ecf5fd',
        borderColor: '#000000',
        borderRadius: 4,
        borderWidth: 2.5,
        marginLeft: 20,
        marginRight: 20,
        marginBottom: 70
      }
    
});
